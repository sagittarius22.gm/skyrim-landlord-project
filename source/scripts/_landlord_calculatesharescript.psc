Scriptname _LandLord_CalculateShareScript extends Quest  


Function CalculateShare()
	Float TownReduc = 1.0
if Player_Habitation.GetLocation().HasKeyword(LocTypeTown)
	TownReduc = _LandLord_SmallTownPriceReduction.Value
endif
if _LandLord_CurrentLocationExtorted == none
	_LandLord_CurrentLocationExtorted = Game.GetFormFromFile(0x03D435,"Landlord.esp") as KEYWORD ; had to do this because I messed up on my test savegame...
endif
	Location Store = Player_Store.GetLocation()
if Store.GetKeyWordData(_LandLord_CurrentLocationOwned) > 0.0
	Bool  EECBonus = _LandLord_EastEmpireContractDone.Value as BOOL
	Float fEECBonus = 0.0

	Bool  SSBonus = _LandLord_ShatterShieldsContractDone.Value as BOOL
	Float fSSBonus = 0.0
		if SSBonus
			if Player_Hold.GetLocation().GetKeyWordData(CWOwner) == CWSons.Value
				fSSBonus = _LandLord_SSBonus.Value
			endif
		endif

		if EECBonus 
			if Player_Hold.GetLocation().GetKeyWordData(CWOwner) == CWImperial.Value
				fEECBonus = _LandLord_EECBonus.Value
			endif
		endif
	Float DaysSinceLastVisit =  GameDaysPassed.Value - Store.GetKeyWordData(_LandLord_GameDaysPassedSinceLastVisit)
	Store.SetKeyWordData(_LandLord_CurrentPropertyAwaitingShare, Math.Floor(Utility.RandomFloat(_LandLord_DailyShareMin.Value, _LandLord_DailyShareMax.Value)* DaysSinceLastVisit*TownReduc+(50*fSSBonus)+(50*fEECBonus) ))
elseif Store.GetKeyWordData(_LandLord_CurrentLocationExtorted) > 0.0
	Float DaysSinceLastVisit =  GameDaysPassed.Value - Store.GetKeyWordData(_LandLord_GameDaysPassedSinceLastVisit)
	Store.SetKeyWordData(_LandLord_CurrentPropertyAwaitingShare, Math.Floor(Utility.RandomFloat(_LandLord_DailyShare_ExtortedMin.Value, _LandLord_DailyShare_ExtortedMax.Value)* DaysSinceLastVisit*TownReduc))
endif
endFunction

Function GiveShare()	
	Location Store = Player_Store.GetLocation()
	Game.GetPlayer().AddItem(Gold001, Store.GetKeyWordData(_LandLord_CurrentPropertyAwaitingShare) as INT)
	Store.SetKeyWordData(_LandLord_CurrentPropertyAwaitingShare, 0.0)
	Store.SetKeyWordData(_LandLord_GameDaysPassedSinceLastVisit, GameDaysPassed.Value)
endFunction

MiscObject Property Gold001 Auto

GlobalVariable Property _LandLord_DailyShare_ExtortedMax Auto
GlobalVariable Property _LandLord_DailyShare_ExtortedMin Auto
GlobalVariable Property GameDaysPassed Auto
GlobalVariable Property _LandLord_DailyShareMax Auto 
GlobalVariable Property _LandLord_DailyShareMin Auto 
GlobalVariable Property _LandLord_SmallTownPriceReduction Auto

Keyword Property CWOwner Auto
Keyword Property LocTypeTown Auto
Keyword Property _LandLord_CurrentLocationOwned Auto
Keyword Property _LandLord_CurrentLocationExtorted Auto
Keyword Property _LandLord_CurrentPropertyAwaitingShare Auto
Keyword Property _LandLord_GameDaysPassedSinceLastVisit Auto

GlobalVariable Property CWImperial Auto
GlobalVariable Property CWSons Auto
GlobalVariable Property _LandLord_EECBonus Auto
GlobalVariable Property _LandLord_EastEmpireContractDone Auto
GlobalVariable Property _LandLord_SSBonus Auto
GlobalVariable Property _LandLord_ShatterShieldsContractDone Auto

LocationAlias Property Player_Hold Auto
LocationAlias Property Player_Store Auto
LocationAlias Property Player_Habitation Auto