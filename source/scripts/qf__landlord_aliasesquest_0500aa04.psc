;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 2
Scriptname QF__LandLord_AliasesQuest_0500AA04 Extends Quest Hidden

;BEGIN ALIAS PROPERTY Player_TownCity
;ALIAS PROPERTY TYPE LocationAlias
LocationAlias Property Alias_Player_TownCity Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY Player_Store
;ALIAS PROPERTY TYPE LocationAlias
LocationAlias Property Alias_Player_Store Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY Alias_Player
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_Alias_Player Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY Player_Hold
;ALIAS PROPERTY TYPE LocationAlias
LocationAlias Property Alias_Player_Hold Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY Store_Owner
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_Store_Owner Auto
;END ALIAS PROPERTY

;BEGIN FRAGMENT Fragment_0
Function Fragment_0()
;BEGIN AUTOCAST TYPE _LandLord_CalculateShareScript
Quest __temp = self as Quest
_LandLord_CalculateShareScript kmyQuest = __temp as _LandLord_CalculateShareScript
;END AUTOCAST
;BEGIN CODE
kMyQuest.CalculateShare()
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
