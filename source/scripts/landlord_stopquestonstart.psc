Scriptname LandLord_StopQuestOnStart extends Quest  


Quest Property OtherQuest Auto

Event OnStoryChangeLocation(ObjectReference akActor, Location akOldLocation, Location akNewLocation)		
	OtherQuest.stop()
	Stop()
endEvent