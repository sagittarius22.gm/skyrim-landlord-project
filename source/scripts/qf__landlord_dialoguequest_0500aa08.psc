;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 5
Scriptname QF__LandLord_DialogueQuest_0500AA08 Extends Quest Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0()
;BEGIN AUTOCAST TYPE _LandLord_BuyPropertyScript
Quest __temp = self as Quest
_LandLord_BuyPropertyScript kmyQuest = __temp as _LandLord_BuyPropertyScript
;END AUTOCAST
;BEGIN CODE
kMyQuest.BuyProperty()
Reset()
SetStage(0)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
