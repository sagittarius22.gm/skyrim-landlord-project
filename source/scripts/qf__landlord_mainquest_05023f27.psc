;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 7
Scriptname QF__LandLord_MainQuest_05023F27 Extends Quest Hidden

;BEGIN ALIAS PROPERTY Player
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_Player Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY CourierNote
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_CourierNote Auto
;END ALIAS PROPERTY

;BEGIN FRAGMENT Fragment_6
Function Fragment_6()
;BEGIN CODE
WICourier.addItemToContainer(_LandLord_CourierLetter02)
;END CODE
EndFunction
;END FRAGMENT 

;BEGIN FRAGMENT Fragment_5
Function Fragment_5()
;BEGIN CODE
SetObjectiveCompleted(10)
SetStage(20)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_4
Function Fragment_4()
;BEGIN CODE
SetObjectiveDisplayed(10)
_LandLord_DialogueQuest.Start()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_2
Function Fragment_2()
;BEGIN CODE
CourierNote.TryToEnable()
WICourier.addAliasToContainer(CourierNote)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

WICourierScript Property WICourier  Auto  

Book Property _LandLord_CourierLetter  Auto  

ReferenceAlias Property CourierNote  Auto  

Quest Property _LandLord_DialogueQuest  Auto  

Book Property _LandLord_CourierLetter02  Auto  
