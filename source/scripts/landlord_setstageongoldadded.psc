Scriptname LandLord_SetStageOnGoldAdded extends ReferenceAlias

MiscObject Property Gold001 Auto
Int Property StageToSet Auto

Event OnItemAdded(Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akSourceContainer)
if GetReference().GetItemCount(Gold001) >= 5000
	GoToState("Empty")
	GetOwningQuest().SetStage(StageToSet)
endif
endEvent

Event OnInit()
if GetReference()
	AddInventoryEventFilter(Gold001)
endif
endEvent

State Empty
	Event OnItemAdded(Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akSourceContainer)
	endEvent

	Event OnInit()
	endEvent
endState