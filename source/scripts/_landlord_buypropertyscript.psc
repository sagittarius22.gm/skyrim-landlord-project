Scriptname _LandLord_BuyPropertyScript extends Quest  

Function ExtortProperty()
if _LandLord_CurrentPropertyExtorted == none
	_LandLord_CurrentPropertyExtorted = Game.GetFormFromFile(0x03D435,"Landlord.esp") as KEYWORD ; had to do this because I messed up on my test savegame...
endif
	Player_Store.GetLocation().SetKeyWordData(_LandLord_CurrentPropertyExtorted, 1.0)
endFunction

Function BuyProperty()
	PropertyTitleQuest.Start()
	_LandLord_OwnedPropertiesList.AddForm(Player_Store.GetLocation())
	Player_Store.GetLocation().SetKeyWordData(_LandLord_CurrentLocationOwned, 1.0)
	Game.GetPlayer().RemoveItem(Gold001, _LandLord_CurrentLocationPrice.Value as INT)
if _LandLord_MainQuest.GetStage() == 10
	_LandLord_MainQuest.SetStage(15)
endif
	Store_Owner.GetActorReference().SetRelationShipRank(Game.GetPlayer(), Store_Owner.GetActorReference().GetRelationShipRank(Game.GetPlayer()) + 1)
endFunction



int Function Adjust(actor vendor)
	Location PlayerHold = Player_Hold.GetLocation()
	_LandLord_CurrentLocationPrice.Value = _LandLord_StoreBasePrice.Value
if PlayerHold .HaskeyWord(LocTypeHoldMajor)
	_LandLord_CurrentLocationPrice.Value += _LandLord_HoldMajorBasePrice.value
elseif PlayerHold .HaskeyWord(LocTypeHoldMinor)
	_LandLord_CurrentLocationPrice.Value += _LandLord_HoldMinorBasePrice.value
endif
	Actor PlayerRef = Game.GetPlayer()
	Int RelationShipRank = vendor.GetRelationShipRank(PlayerRef)
	Float BarterDelta = (PlayerRef.GetActorValue("Speechcraft") - vendor.GetActorValue("Speechcraft"))
if vendor.GetRace() == PlayerRef.GetRace()
	_LandLord_CurrentLocationPrice.Value -= _LandLord_SameRaceBonus.Value
else
	_LandLord_CurrentLocationPrice.Value += _LandLord_DifferentRaceMalus.Value
		
endif
	_LandLord_CurrentLocationPrice.Value += PlayerRef.GetLevel()*_LandLord_PlayerLevelMalus.Value

	_LandLord_CurrentLocationPrice.Value += _LandLord_RelationShipBonusMalus.Value * RelationShipRank

	_LandLord_CurrentLocationPrice.Value -= BarterDelta * _LandLord_SpeechcraftDeltaBonus.Value

	_LandLord_CurrentLocationPrice.Value += vendor.GetBribeAmount()

	if (_LandLord_StoreCheaperList).HasForm(Player_Store.GetLocation())
		_LandLord_CurrentLocationPrice.Value =  ((_LandLord_CurrentLocationPrice.Value)/1.5) as INT
	endif
	

	if Player_Habitation.GetLocation().HasKeyword(LocTypeTown)
		_LandLord_CurrentLocationPrice.Value = _LandLord_CurrentLocationPrice.Value * _LandLord_SmallTownPriceReduction.value
	endif

	if _LandLord_CurrentLocationPrice.Value <= 0.0
			_LandLord_CurrentLocationPrice.Value = 1500.0
	endif

	UpdateCurrentInstanceGlobal(_LandLord_CurrentLocationPrice)

	_LandLord_DialogueQuest.UpdateCurrentInstanceGlobal(_LandLord_CurrentLocationPrice)

			

	;Debug.Notification("Price should be " +_LandLord_CurrentLocationPrice.Value as INT)
	Store_Owner.GetOwningQuest().UpdateCurrentInstanceGlobal(_LandLord_CurrentLocationPrice)
	;_LandLord_DebugMessage.Show()
	Return (_LandLord_CurrentLocationPrice.Value) as INT
endFunction

;int DebugStep = 0

;bool Function Debug()
		;		DebugStep += 1
		;		Debug.MessageBox("Step "+DebugStep+": "+_LandLord_CurrentLocationPrice.Value as INT)
		;		return true
;endFunction

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; PROPERTIES

GlobalVariable Property GameDaysPassed Auto

FormList Property _LandLord_OwnedPropertiesList Auto

Quest Property PropertyTitleQuest Auto

GlobalVariable Property _LandLord_CurrentLocationPrice Auto
MiscObject Property Gold001 Auto

Message Property _LandLord_DebugMessage Auto

Quest Property _LandLord_MainQuest Auto
Quest Property _LandLord_DialogueQuest Auto


GlobalVariable Property _LandLord_SmallTownPriceReduction Auto
GlobalVariable Property _LandLord_PlayerLevelMalus Auto
GlobalVariable Property _LandLord_RelationShipBonusMalus Auto
GlobalVariable Property _LandLord_DifferentRaceMalus Auto
GlobalVariable Property _LandLord_SameRaceBonus Auto
GlobalVariable Property _LandLord_SpeechcraftDeltaBonus Auto

GlobalVariable Property _LandLord_HoldMajorBasePrice Auto
GlobalVariable Property _LandLord_HoldMinorBasePrice Auto
GlobalVariable Property _LandLord_StoreBasePrice Auto

FormList Property _LandLord_StoreCheaperList  Auto  

ReferenceAlias Property Store_Owner Auto
LocationAlias Property Player_Store Auto
LocationAlias Property Player_Hold Auto
LocationAlias Property Player_Habitation Auto

Keyword Property LocTypeTown Auto 
Keyword Property LocTypeCity Auto
Keyword Property _LandLord_CurrentPropertyExtorted Auto
Keyword Property _LandLord_CurrentPropertyAwaitingShare Auto
Keyword Property _LandLord_GameDaysPassedSinceLastVisit Auto
Keyword Property LocTypeHoldMajor Auto
Keyword Property LoctypeHoldMinor Auto
Keyword Property _LandLord_CurrentLocationOwned Auto

;;;;;;;;;;; DEPRECATED
;GlobalVariable Property LocationPrice Auto

