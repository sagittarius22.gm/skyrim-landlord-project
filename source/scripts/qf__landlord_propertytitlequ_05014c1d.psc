;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 3
Scriptname QF__LandLord_PropertyTitleQu_05014C1D Extends Quest Hidden

;BEGIN ALIAS PROPERTY Store_Owner
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_Store_Owner Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY Player
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_Player Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY Player_Store
;ALIAS PROPERTY TYPE LocationAlias
LocationAlias Property Alias_Player_Store Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY Player_Hold
;ALIAS PROPERTY TYPE LocationAlias
LocationAlias Property Alias_Player_Hold Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY PropertyTitle
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_PropertyTitle Auto
;END ALIAS PROPERTY

;BEGIN FRAGMENT Fragment_0
Function Fragment_0()
;BEGIN CODE
Utility.Wait(1.0)
Stop()
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
